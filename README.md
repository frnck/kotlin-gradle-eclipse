### Gradle / Kotlin / Eclipse reference project

This is a working example project if you want to use Eclipse, Gradle and Kotlin. Please share the good news.

Tested with:
- Oracle JDK 8, http://www.oracle.com/technetwork/java/javase/downloads/index.html
- Eclipse Oxygen.3 Release (4.7.3), https://www.eclipse.org/oxygen/
- Kotlin Plugin for Eclipse, see https://marketplace.eclipse.org/content/kotlin-plugin-eclipse
- Buildship, https://projects.eclipse.org/projects/tools.buildship

Remarks:
- build.gradle in the project root contains Eclipse classpath fixes
- gradle 4.0. Gradle 4.6 is known to break because of problems with code layout

References:
- https://kotlinlang.org/docs/reference/using-gradle.html.